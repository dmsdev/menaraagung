<?php
namespace MenaraAgung;

use GuzzleHttp\Client as GuzzleClient;

use Illuminate\Http\Request;

class Client {
	use URLBuilder;

    protected $url				= null;

    protected $oauth_token      = 'oauth/token';

    protected $oauth_authoration= '/oauth/authorize';

    protected $public_prefix    = 'public';

    protected $callback         = 'http://localhost/ci_auth/public/home';

    protected $scopes           = '*';

    protected $client_option    = [];

    protected $client_secret    = '';

    protected $clientID         = '';

    protected $request          = null;

    protected $token_object     = null;

    public function __construct($url = null)
	{
		$this->request = new Request;
		
        if ($url != null) $this->url = $url;
        
        if($this->url == null){
        	throw new \Exception('API ENDPOINT is not set !!!');
		}
    }

    public function fecthAccessToken()
    {
        try {
            $http = new GuzzleClient;
            $response = $http->post($this->URIBuilder($this->oauth_token), [
                'form_params' => array_merge([
                    'grant_type' 	=> 'authorization_code',
                    'client_id'     => $this->clientID,
                    'client_secret' => $this->client_secret,
                    'redirect_uri'  => $this->callback,
                    'code'          => $this->request->code,
                ], $this->client_option),
            ]);
	
			$this->token_object = json_decode((string) $response->getBody());

        }catch (\Exception $exception){
            if ($exception->getCode() == 400){
                return $this->requestNewCode();
            }
        }

        return $this;
    }

	public function URLNewCode(){
		$query = http_build_query([
			'client_id'     => $this->clientID,
			'redirect_uri'  => $this->callback,
			'response_type' => 'code',
			'scope'         => $this->scopes
		]);

		return $this->URIBuilder( $this->oauth_authoration) . '?'. $query;
	}

    public function requestNewCode()
    {
        return redirect()->away($this->URLNewCode());
    }

    public function refreshToken(){
        $http = new GuzzleClient;

        $response = $http->post($this->URIBuilder($this->oauth_token), [
            'form_params' => [
                'grant_type'    => 'refresh_token',
                'refresh_token' => $this->token_object->refresh_token,
                'client_id'     => $this->clientID,
                'client_secret' => $this->client_secret,
                'scope'         => '',
            ],
        ]);

		$this->token_object = json_decode((string) $response->getBody());

		return $this;
    }

    public function token($key = null){
		if ($key == null) {
			return $this->token_object;
		}

		if(is_array($this->token_object)){
			return $this->token_object[$key];
		}

		return $this->token_object->{$key};
	}
	
	public function set($name, $value = null){
    	if(is_array($name)){
    		foreach ($name as $k => $v){
    			$this->set($k, $v);
			}
    		return $this;
		}
    	
    	$this->{$name} = $value;
    	
    	return $this;
	}
}
