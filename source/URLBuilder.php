<?php

namespace MenaraAgung;


trait URLBuilder
{
	public function URIBuilder($endpoint = null){
		$url = $this->url;
		
		if($this->public_prefix != null){
			$url .= '/' . $this->public_prefix;
		}
		
		if($endpoint != null){
			$url .= '/' . $endpoint;
		}
		
		return $url;
	}
}